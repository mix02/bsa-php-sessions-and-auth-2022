<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ArticleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $slug = Str::slug($this->faker->text(), '-');
        return [
            'title' => $this->faker->text(20),
            'body' => $this->faker->text(),
            'slug' => $slug,
            'user_id' => User::all()->random()->id
        ];
    
    }
}
