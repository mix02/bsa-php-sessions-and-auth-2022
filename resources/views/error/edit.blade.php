@extends('layouts.app')

@section('content')

<div class="container">
    <div class="alert alert-danger" role="alert">
        You can't edit conter your satus is {{ $user->role }}    
    </div>
    <div class="alert alert-danger" role="alert">
        Article status is {{ $article->status }}
    </div>
    <div class="alert alert-danger" role="alert">
        You are not owner of this content !  
    </div>
</div>

@endsection