@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <h3>Create</h3>
        </div>
        <div class="row">
            <form method="POST" action="{{ route('article.store') }}">
                @csrf
                <div class="mb-3">
                    <label for="title">Title</label>
                    <input type="text" name="title" id="title" class="form-control"
                        value="{{ isset($article) ? $article->title : '' }}" />
                </div>

                <div class="mb-3">
                    <label for="body">Body</label>
                    <textarea name="body" id="body"
                        class="form-control">{{ isset($article) ? $article->body : '' }}</textarea>
                </div>

                <div class="form-group">
                    <label>Select Role</label>
                    <select class="form-control" name="status">
                        @foreach ($statuses as $id => $status)
                            <option value="{{ $status }}" {{ $status == old('status') ? 'selected' : '' }}>
                                {{ $status }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

@endsection
