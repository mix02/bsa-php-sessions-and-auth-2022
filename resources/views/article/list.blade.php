@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Body</th>
                        <th scope="col">Status</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($articles as $article)
                        @if(auth()->user()->role === 'creator')
                            @if ($article->status === 'under_moderation')
                                @continue
                            @endif
                            <tr>
                                <td scope="row">
                                    {{ $article->id }}
                                </td>
                                <td>
                                    {{ $article->title }}
                                </td>
                                <td>
                                    {{ strlen($article->body) > 50 ? substr($article->body, 0, 100) . '...' : $article->body }}
                                </td>
                                <td>
                                    {{ $article->status }}
                                </td>
                                <td>
                                    <a href="{{ route('article.edit', $article) }}">Edit</a>
                                </td>
                            </tr>
                        @endif
                            <tr>
                                <td scope="row">
                                    {{ $article->id }}
                                </td>
                                <td>
                                    {{ $article->title }}
                                </td>
                                <td>
                                    {{ strlen($article->body) > 50 ? substr($article->body, 0, 100) . '...' : $article->body }}
                                </td>
                                <td>
                                    {{ $article->status }}
                                </td>
                                <td>
                                    <a href="{{ route('article.edit', $article) }}">Edit</a>
                                </td>
                            </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
