@extends('layouts.app')
<style>
    .inner-li {
        display: inline-block;
        margin-right: 10px;
        vertical-align: middle;
    }
</style>
@section('content')
    <div class="container">
        <div class="row">
            <h2>List of Users: </h2>
            <ul>
                @foreach ($users as $i => $user)
                    <form action="{{ route('admin.update', $user) }}" method="post">
                        <li>
                            <strong>Name: {{ $user->name }}</strong>
                            @csrf
                            <ul>
                                @foreach ($roles as $key => $role)
                                    <li class="inner-li">
                                        <input class="check-list" type="checkbox" id="{{ 'role' . $i . $key }}" name="role"
                                            value="{{ $role }}">
                                        <label for="{{ 'role' . $i . $key }}">{{ $role }}</label>
                                    </li>
                                @endforeach
                            </ul>

                        </li>
                        <button type="submit" class="btn btn-primary">Press</button>
                    </form>
                @endforeach
            </ul>
        </div>
    </div>

    <script>

        $('.check-list').on('change', function() {
		    $('.check-list').not(this).prop('checked', false);  
		});
    </script>



@endsection
