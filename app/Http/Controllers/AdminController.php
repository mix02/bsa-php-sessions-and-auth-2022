<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function index()
    {
        $users = User::all();
        $roles = User::getRoles();

        return view('admin.index', compact('users', 'roles'));
    }

    public function update(Request $request, User $user)
    {

        $validated = $request->validate([
            'role' => 'required|string'
        ]);
        
        $user->role = $validated['role'] ?? $user->role;

        $user->update();
        return redirect()->route('admin.index');

    }
}
