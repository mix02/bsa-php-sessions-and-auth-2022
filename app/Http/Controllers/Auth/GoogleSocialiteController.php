<?php

namespace App\Http\Controllers\Auth;
use Exception;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Socialite;

class GoogleSocialiteController extends Controller
{
    
    public function redirectToGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    public function handleCallback()
    {
        try {
     
            $user = Socialite::driver('google')->user();
            
            // dd('User ', $user);
            $googleUser = User::where('social_id', $user->id)->first();
      
            if($googleUser){
      
                Auth::login($googleUser);
     
                return redirect('/home');
      
            }else{
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'social_id'=> $user->id,
                    'social_type'=> 'google',
                    'password' => encrypt('google')
                ]);
                
                Auth::login($newUser);
      
                return redirect()->route('home');
            }
     
        } catch (Exception $e) {
            dd('Error ' . $e->getMessage());
        }
    }
}
