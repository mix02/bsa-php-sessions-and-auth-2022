<?php

namespace App\Providers;

use App\Models\Article;
use App\Policies\ArticlePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
        Article::class => ArticlePolicy::class,
        
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
   
        // Defines a user role
        Gate::define('isAdmin', function($user) {
           return $user->role == 'admin';
        });
       
        Gate::define('isModerator', function($user) {
            return $user->role == 'moderator';
        });
      
        Gate::define('isCreator', function($user) {
            return $user->role == 'creator';
        });
    }
}
