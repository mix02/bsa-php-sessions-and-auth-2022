<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\Auth\GoogleSocialiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function ($router) {
    Route::resource('article', ArticleController::class, ['except' => ['show']]);
    Route::get('/admin', [AdminController::class, 'index'])->name('admin.index');
    Route::post('/admin/update/{user}', [AdminController::class, 'update'])->name('admin.update');
});


Route::get('/article/{article}', [ArticleController::class, 'show'])->name('article.show');

Route::get('auth/google', [GoogleSocialiteController::class, 'redirectToGoogle'])->name('auth.google');
Route::get('google/callback', [GoogleSocialiteController::class, 'handleCallback']);


